let Bicicleta = function (id,color,modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion
}

Bicicleta.prototype.toString = function (){
    return `id:${this.id} | color: ${this.color}`;
}

Bicicleta.allBicicletas = [];

Bicicleta.add =function (aBicicleta){
    Bicicleta.allBicicletas.push(aBicicleta)
}
Bicicleta.findById = function(idBici){
    var encontrado = Bicicleta.allBicicletas.find(x=>x.id==idBici)
    return encontrado?encontrado:false;
}

Bicicleta.removeBici=function(idBici){
    
    var bici = Bicicleta.findById(idBici)
    if(bici){
        for (let index = 0; index < Bicicleta.allBicicletas.length; index++) {
            if(Bicicleta.allBicicletas[index].id == bici.id){
                Bicicleta.allBicicletas.splice(index,1)
                break;
            }
            
        }

    }
}

Bicicleta.updateBici = function (newBici){
    
    for (let index = 0; index < Bicicleta.allBicicletas.length; index++) {
        if(newBici.id ==Bicicleta.allBicicletas[index].id){
           
            Bicicleta.allBicicletas[index].color = newBici.color;
            Bicicleta.allBicicletas[index].modelo = newBici.modelo;
            Bicicleta.allBicicletas[index].ubicacion = newBici.ubicacion;
            
        }
        
    }
    

}


let Bici1 = new Bicicleta(1,'rojo','urbana',['36.634718', '-81.790600'])
let Bici2 = new Bicicleta(2,'verde','campo',['36.636233', '-81.787768'])
let Bici3 = new Bicicleta(3,'negro','campo',['36.634270', '-81.784614'])
Bicicleta.add(Bici1)
Bicicleta.add(Bici2)
Bicicleta.add(Bici3)


module.exports = Bicicleta;