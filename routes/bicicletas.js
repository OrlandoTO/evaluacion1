let express = require('express')
let router = express.Router()
let bicicletaController = require('../controllers/bicicleta')
router.get('/',bicicletaController.bicicleta_list)
router.get('/create',bicicletaController.bicicleta_create)
router.post('/store',bicicletaController.bicicleta_store)
router.post('/:id/delete',bicicletaController.bicicleta_delete)
router.get('/:id/show',bicicletaController.bicicleta_show)
router.post('/update',bicicletaController.bicicleta_update)

module.exports = router